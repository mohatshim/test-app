import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title = "CodeSandbox";
  headers = ["Name","Email","Addres",];
  column = ["name","email","address"];
  data = [
    {name:"Sumit",email:"sumit@gmail.com",address:"Agra"},
  {name:"Sumit",email:"sumit@gmail.com",address:"Agra"},
  {name:"Manan",email:"manan@gmail.com",address:"Delhi"},
  {name:"Shivam",email:"shivam@gmail.com",address:"Bangalore"},
  {name:"Naman",email:"naman@gmail.com",address:"Mysore"}]
  
  res:any[]=[];
  constructor(private s :HomeService,private http:HttpClient) { }

  ngOnInit(): void {
    this.res = this.data;
  }
  
 filter(type:string,value:any){
  console.log('tgus ',type)
  console.log('value ',value.target.value)
  this.res = [];
  switch(type){
    case 'Name':this.data.forEach(e=>{
                if(e.name === value.target.value)
                this.res.push(e); });break;
    case 'Email':this.data.forEach(e=>{
      if(e.email === value.target.value)
      this.res.push(e); });break;
    case 'Addres':this.data.forEach(e=>{
        if(e.address === value.target.value)
        this.res.push(e); });break;
  }
  
  
 }
}
